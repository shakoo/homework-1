//
//  HWAnimationConstants.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 03/10/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#ifndef HWAnimationConstants_h
#define HWAnimationConstants_h

/*
 * Константы используемые в задании №3;
 * Можно менять для изменения анимации (на свой страх и риск).
 */

long const HWDefaultPointsThreshold = 50; // Количество точек в "хвосте";
double const HWDefaultPointSize = 8.0f; // Размер точки;

#endif /* HWAnimationConstants_h */
